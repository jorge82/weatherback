/* Function que transforma el formato de respuesta de clima actual */
function adaptCurrentWeatherFormat(weatherData) {
  let fecha = {};
  if (weatherData.dt_txt) {
    fecha.date = new Date(Date.parse(weatherData.dt_txt)).toLocaleDateString();
    fecha.day = new Date(Date.parse(weatherData.dt_txt)).toLocaleDateString("es-US", { weekday: "long" });
  } else {
    fecha.date = new Date(Date.now()).toLocaleDateString();
    fecha.day = new Date(Date.now()).toLocaleDateString("es-US", { weekday: "long" });
  }
  const weather = {
    city: weatherData.name,
    lon: weatherData.coord.lon,
    lat: weatherData.coord.lat,
    date: fecha.date,
    day: fecha.day,
    temp: weatherData.main.temp,
    temp_max: weatherData.main.temp_max,
    temp_min: weatherData.main.temp_min,
    humidity: weatherData.main.humidity,
    weather: weatherData.weather[0].main
  };
  return weather;
}
/* Function que transforma el formato de respuesta del pronostico de clima */

function adaptWeatherFormat(weatherData) {
  let fecha = {};
  if (weatherData.dt) {
    fecha.date = new Date(weatherData.dt * 1000).toLocaleDateString();
    fecha.day = new Date(weatherData.dt * 1000).toLocaleDateString("es-US", { weekday: "long" });
  } else {
    fecha.date = new Date(Date.now()).toLocaleDateString();
    fecha.day = new Date(Date.now()).toLocaleDateString("es-US", { weekday: "long" });
  }

  const weather = {
    date: fecha.date,
    day: fecha.day,
    temp: weatherData.temp.day,
    humidity: weatherData.humidity,
    pressure: weatherData.pressure,
    weather: weatherData.weather[0].main
  };
  return weather;
}

module.exports.adaptCurrentWeatherFormat = adaptCurrentWeatherFormat;
module.exports.adaptWeatherFormat = adaptWeatherFormat;
