# Enunciado
Summary
Las tecnologías a utilizar son NodeJS, ReactJS o React Native. En el caso de preferir RN, utilizarlo de
manera nativa (sin expo).
La siguiente prueba plantea el desarrollo de una aplicación de consulta de clima que pueda visualizar el
pronóstico actual, próximos 5 días para la ciudad actual y de otras 5 ciudades seleccionables.
Además del desarrollo específico de las funcionalidades, se requiere identificar y generar los casos de
test que se consideren necesarios
La entrega del código se espera que se entregue en algún repositorio público (Por ejemplo Github).
Para aquellos perfiles que apliquen como FullStack, se deben completar ambas partes de la prueba. Caso
contrario, pueden solo realizar la parte que corresponde (armar la solución entera puede ser un extra).
Se recomienda utilizar servicio API de weather Open Weather Map, pero se puede usar utilizar cualquier
otro de preferencia.
Sugerencia de servicios/librerías:
• Supertest (recomendado)
• Should (recomendado)
• ip-api (recomendado)
Se pueden usar cualquier otras librerías que consideren de utilidad, aunque recomendamos el uso de
Redux y Redux-Saga. Cualquier información relevante la deben agregar al Readme del proyecto.
Tomate tu tiempo, y demuestra tus conocimientos pensando en la estructura y el trabajo en equipo.

# Backend Test
Preferentemente desarrollar sobre NodeJS.
Se requiere implementar una API que provea en formato JSON el estado del tiempo basado en
diferentes endpoints.
Se requiere realizar tests con las librerías antes mencionadas o con equivalentes.
A continuación se detallan los endpoints que deben ser implementados
Ruta base
/v1
Endpoints
/location
Devuelve los datos de ubicación city según ip-api.
/current[/city]
City es un parámetro opcional. Devuelve los datos de ubicación city o la ubicación actual según
ip-api y el estado del tiempo actual.
/forecast[/city]
City es un parámetro opcional. Devuelve los datos de ubicación city o la ubicación actual según
ip-api y el estado del tiempo a 5 días

# Install
npm  install

# Run tests
npm test

# Run 
npm start

# Formato de respuestas

## .../v1/location/
### Ejepmplo : http://weathertjorgeapp.herokuapp.com/v1/location
{city:"Buenos Aires"}
## .../v1/current/
### Ejemplo London: .../v1/current/london

  {"city":"London","lon":-0.1257,"lat":51.5085,"date":"29/10/2021","day":"viernes","temp":11.66,"temp_max":12.92,"temp_min":10.07,"humidity":83,"weather":"Clouds"}
### Ejemplo ciudad no existente: .../v1/current/ss
   Error: La ciudad no existe

## /v1/forecast/
### Ejemplo London: .../v1/forecast/paris

 {"city":"Paris","lat":48.8534,"lon":2.3488,"data":[{"date":"30/10/2021","day":"sábado","temp":13.98,"humidity":89,"pressure":1000,"weather":"Rain"},{"date":"31/10/2021","day":"domingo","temp":16.27,"humidity":77,"pressure":1003,"weather":"Rain"},{"date":"1/11/2021","day":"lunes","temp":14.31,"humidity":53,"pressure":1003,"weather":"Rain"},{"date":"2/11/2021","day":"martes","temp":12.5,"humidity":65,"pressure":997,"weather":"Rain"},{"date":"3/11/2021","day":"miércoles","temp":12.31,"humidity":61,"pressure":1004,"weather":"Rain"}]}
### Ejemplo ciudad no existente : .../v1/forecast/xxxx
   Error: La ciudad no existe