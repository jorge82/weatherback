const express = require("express");
var cors = require("cors");
const app = express();
app.use(cors());

require("dotenv").config();
const locationRoutes = require("./routes/locationRoutes");
const weatherRutes = require("./routes/weatherRoutes");

const PORT = process.env.PORT || 4000;

app.get("/", (req, res) => {
  res.send("Weather service running Ok");
});
/* Rutas */
app.use("/v1", locationRoutes);
app.use("/v1", weatherRutes);

/* Error handler */
app.use((error, req, res, next) => {
  return res.status(500).send(error.toString());
});

app.listen(PORT, () => {
  console.log(`Server listening at http://localhost:${PORT}`);
});

module.exports = app;
