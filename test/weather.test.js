const request = require("supertest");
const app = require("../index");
var assert = require("assert");

describe("Obtener clima londres", () => {
  test("GET /v1/current", (done) => {
    request(app)
      .get("/v1/current/london")
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        return done();
      });
  });
});

describe("Obtener clima  buenos Aires", () => {
  test("GET /v1/current", (done) => {
    request(app)
      .get("/v1/current/buenos aires")
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        return done();
      });
  });
});

describe("Obtener pronostico buenos Aires 5 dias, devuelve 5 valores", () => {
  test("GET /v1/forecast", (done) => {
    request(app)
      .get("/v1/forecast/buenos aires")
      .expect("Content-Type", /json/)
      .expect(200)
      .then((response) => {
        assert.equal(response.body.data.length, 5);
        done();
      })
      .catch((err) => done(err));
  });
});

describe("Obtener pronostico tokio 5 dias, devuelve 5 valores", () => {
  test("GET /v1/forecast", (done) => {
    request(app)
      .get("/v1/forecast/tokio")
      .expect("Content-Type", /json/)
      .expect(200)
      .then((response) => {
        assert.equal(response.body.data.length, 5);
        done();
      })
      .catch((err) => done(err));
  });
});
describe("Obtener pronostico ciudad inexistente 5 dias, devuelve error 500", () => {
  test("GET /v1/forecast", (done) => {
    request(app)
      .get("/v1/forecast/xxx")
      .expect(500)
      .end((err, res) => {
        if (err) return done(err);
        return done();
      });
  });
});
