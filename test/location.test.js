const request = require("supertest");
const getCityByIp = require("../api/ip-api");
const { ERROR_CONEXION } = require("../const/errorsConts");

describe("Test  getCityByIp", () => {
  test("la ciudad con ip 103.136.43.0 es moscow", () => {
    return getCityByIp("103.136.43.0").then((data) => {
      expect(data).toBe("Moscow");
    });
  });
});
describe("Test  getCityByIp", () => {
  test("La ciudad con ip 161.185. 160.93 es Brooklyn", () => {
    return getCityByIp("161.185.160.93").then((data) => {
      expect(data).toBe("Brooklyn");
    });
  });
});
describe("Test  ip invalidad getCityByIp", () => {
  test("", () => {
    return getCityByIp().catch((e) => expect(e).toMatch(ERROR_CONEXION));
  });
});
