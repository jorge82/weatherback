const ERROR_CONEXION = "Falla  de conexion";
const ERROR_CIUDAD_NO_EXISTE = "La ciudad no existe";

module.exports.ERROR_CONEXION = ERROR_CONEXION;
module.exports.ERROR_CIUDAD_NO_EXISTE = ERROR_CIUDAD_NO_EXISTE;
