const axios = require("axios");
const { ERROR_CONEXION, ERROR_CIUDAD_NO_EXISTE } = require("../const/errorsConts");
const { adaptCurrentWeatherFormat, adaptWeatherFormat } = require("../helper/responseAdapter");
require("dotenv").config();
const APIKEY = process.env.WEATHER_API_KEY;

/*
 Ver readme.md para ver los formatos de respuesta
*/

/* Función que devuelve el clima actual de la ciudad*/
async function getCurrentWeather(city) {
  return axios
    .get("https://api.openweathermap.org/data/2.5/weather", {
      params: {
        q: city,
        appid: APIKEY,
        units: "metric"
      }
    })
    .then((response) => {
      return adaptCurrentWeatherFormat(response.data);
    })
    .catch((e) => {
      throw new Error(ERROR_CIUDAD_NO_EXISTE);
    });
}
/* Función que devuelve las coordenadas de la ciudad ingresada como parametro
pre:ciudad
post: coordenadas ={city:xxx , lat:yyy ,lon:zzz} si la ciudad existe o lanza una exception ERROR_CIUDAD_NO_EXISTE*/

function getCityCoordinates(city) {
  return axios
    .get("http://api.openweathermap.org/geo/1.0/direct?", {
      params: {
        q: city,
        appid: APIKEY,
        limit: 1 // numero de ubicaciones , limit<=5
      }
    })
    .then((response) => {
      const coordenadas = { city: response.data[0].name, lat: response.data[0].lat, lon: response.data[0].lon };
      return coordenadas;
    })
    .catch((e) => {
      throw new Error(ERROR_CIUDAD_NO_EXISTE);
    });
}
/* Función que devuelve el pronostico de la ciudad ingresada 
pre:ciudad, latitud , longitud
post:  {"city":xxxx,"lat":yyyy,"lon":zzzz,"data":[...] si la ciudad existe o lanza una exception ERROR_CIUDAD_NO_EXISTE*/
function getForcastFiveDays(city, lat, lon) {
  return axios
    .get("https://api.openweathermap.org/data/2.5/onecall", {
      params: {
        lat: lat,
        lon: lon,
        exclude: "hourly,minutely,alerts,current",
        units: "metric",
        appid: APIKEY
      }
    })
    .then((response) => {
      let data = response.data.daily;
      let forecastList = [];
      for (let i = 1; i < data.length - 2; i++) {
        forecastList.push(adaptWeatherFormat(data[i], city));
      }

      return { city: city, lat: lat, lon: lon, data: forecastList };
    })
    .catch((e) => {
      throw new Error(ERROR_CIUDAD_NO_EXISTE);
    });
}
module.exports.getCurrentWeather = getCurrentWeather;
module.exports.getForcastFiveDays = getForcastFiveDays;
module.exports.getCityCoordinates = getCityCoordinates;
