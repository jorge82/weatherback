const axios = require("axios");
const { ERROR_CONEXION } = require("../const/errorsConts");

async function getCityByIp(ip) {
  return axios
    .get("http://ip-api.com/json/" + ip)
    .then((response) => {
      return response.data.city;
    })
    .catch((e) => {
      throw new Error(ERROR_CONEXION);
    });
}

module.exports = getCityByIp;
