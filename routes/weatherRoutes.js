var express = require("express");

const { getCurrentWeather, getForcastFiveDays, getCityCoordinates } = require("../api/weather");
const RequestIp = require("@supercharge/request-ip");
const getCityByIp = require("../api/ip-api");
var router = express.Router();

router.get("/current/:city?", async (req, res, next) => {
  try {
    let city = req.params.city;
    if (!city) {
      const ipCliente = RequestIp.getClientIp(req);
      city = await getCityByIp(ipCliente);
    }
    const weather = await getCurrentWeather(city);
    res.status(200).json(weather);
  } catch (e) {
    next(e);
  }
});
/* Endpoint que devuelve el pronostico de clima por 
  5 dias  de la ciudad pasada por parametro opcional 
  o el de la ciudad de la ip del cliente*/
router.get("/forecast/:city?", async (req, res, next) => {
  try {
    let city = req.params.city;
    if (!city) {
      const ipCliente = RequestIp.getClientIp(req);
      city = await getCityByIp(ipCliente);
    }
    const coord = await getCityCoordinates(city);
    const forecast = await getForcastFiveDays(coord.city, coord.lat, coord.lon);
    res.status(200).json(forecast);
  } catch (e) {
    next(e);
  }
});

module.exports = router;
