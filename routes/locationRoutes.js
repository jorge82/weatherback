var express = require("express");
const getCityByIp = require("../api/ip-api");

var router = express.Router();
const RequestIp = require("@supercharge/request-ip");
router.get("/location", async (req, res, next) => {
  try {
    const ipCliente = RequestIp.getClientIp(req);
    const city = await getCityByIp(ipCliente);
    res.status(200).json({ city: city });
  } catch (e) {
    next(e);
  }
});

module.exports = router;
